﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MPSMWeb.Data;
using MPSMWeb.Models;
using System.Collections.Generic;
using System.Diagnostics;
using Teploobmen.lib;

namespace MPSMWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationContext _context;
        private int _userId;
        public HomeController(ILogger<HomeController> logger, ApplicationContext applicationContext)
        {
            _logger = logger;
            _context = applicationContext;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int.TryParse(User.FindFirst("Id")?.Value, out _userId);
        }

        [HttpPost]
        public IActionResult Result(TeploobmenInput input)
        {
            if (!string.IsNullOrEmpty(input.Name))
            {
                var existVariant = _context.Variants.FirstOrDefault(x => x.Name == input.Name);
                if (existVariant != null)
                {
                        existVariant.H = input.H;
                        existVariant.InitTempOfMaterial = input.InitTempOfMaterial;
                        existVariant.InitTempOfGas = input.InitTempOfGas;
                        existVariant.GasVelocity = input.GasVelocity;
                        existVariant.AverageHeatOfGas = input.AverageHeatOfGas;
                        existVariant.MaterialConsumption = input.MaterialConsumption;
                        existVariant.HeatOfMaterial = input.HeatOfMaterial;
                        existVariant.HeatCoefficient = input.HeatCoefficient;
                        _context.Variants.Update(existVariant);
                        _context.SaveChanges();
                }
                else
                {
                    var variant = new Variant
                    {
                        Name = input.Name,
                        H = input.H,
                        InitTempOfMaterial = input.InitTempOfMaterial,
                        InitTempOfGas = input.InitTempOfGas,
                        GasVelocity = input.GasVelocity,
                        AverageHeatOfGas = input.AverageHeatOfGas,
                        MaterialConsumption = input.MaterialConsumption,
                        HeatOfMaterial = input.HeatOfMaterial,
                        HeatCoefficient = input.HeatCoefficient,
                        UserId = _userId
                    };
                    _context.Variants.Add(variant);
                    _context.SaveChanges();
                }
            }
            var lib = new TeploobmenLib(input);
            var result = lib.Solve();
            if (ModelState.IsValid)  
                return View(result);
            return RedirectToAction("Index");        
        }

        [HttpGet]
        public IActionResult Index(int? variantId)
        {
            var viewModel = new HomeViewModel();
            if(variantId != null)
            {
                viewModel.Variant = _context.Variants
                    .Where(x => x.UserId == _userId || x.UserId == null)
                    .FirstOrDefault(x=>x.Id== variantId);
            }

            viewModel.Variants = _context.Variants
            .Where(x=>x.UserId == _userId || x.UserId == null)
            .ToList();
            return View(viewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Remove(int? variantId)
        {
            var variant = _context.Variants.FirstOrDefault(H => H.Id == variantId);
            
            if (variant != null)
            {
                _context.Variants.Remove(variant);
                _context.SaveChanges();
                TempData["message"] = $"Вариант{variant.Name }удален";
            }
            
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}