﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MPSMWeb.Data;
using MPSMWeb.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using Teploobmen.lib;

namespace MPSMWeb.Controllers
{
    [Authorize]
    public class UserController : Controller
    { 
        public IActionResult Index()
        {   
            return View();
        }
    }
}