﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPSMWeb.Data
{
    public class Variant
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Name { get; set; }
        public double H { get; set; }

        public double InitTempOfMaterial { get; set; }

        public double InitTempOfGas { get; set; }

        public double GasVelocity { get; set; }

        public double AverageHeatOfGas { get; set; }

        public double MaterialConsumption { get; set; }

        public double HeatOfMaterial { get; set; }

        public double HeatCoefficient { get; set; }

        public double DeviceDiameter { get; set; }
       
    }
}
