﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MPSMWeb.Migrations
{
    public partial class FirstMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Email = table.Column<string>(type: "TEXT", nullable: false),
                    Password = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Variants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    H = table.Column<double>(type: "REAL", nullable: false),
                    InitTempOfMaterial = table.Column<double>(type: "REAL", nullable: false),
                    InitTempOfGas = table.Column<double>(type: "REAL", nullable: false),
                    GasVelocity = table.Column<double>(type: "REAL", nullable: false),
                    AverageHeatOfGas = table.Column<double>(type: "REAL", nullable: false),
                    MaterialConsumption = table.Column<double>(type: "REAL", nullable: false),
                    HeatOfMaterial = table.Column<double>(type: "REAL", nullable: false),
                    HeatCoefficient = table.Column<double>(type: "REAL", nullable: false),
                    DeviceDiameter = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variants", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Variants");
        }
    }
}
