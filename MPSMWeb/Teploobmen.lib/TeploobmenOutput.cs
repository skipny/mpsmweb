﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teploobmen.lib
{
    public class TeploobmenOutput
    {
        public List<TeploobmenOutputRow> Rows { get; set; }
    }

        public class TeploobmenOutputRow 
        {
            public double Y { get; set; }
            public double str1 { get; set; }
            public double str2 { get; set; }
            public double str3 { get; set; }
            public double str4 { get; set; }
            public double str5 { get; set; }
            public double str6 { get; set; }
            public double str7 { get; set; }
            public double str8 { get; set; }
        }
}

