﻿using System.Collections.Generic;

namespace Teploobmen.lib
{
    public class TeploobmenLib
    {
        public int H { get; set; }

        public double InitTempOfMaterial { get; set; }

        public double InitTempOfGas { get; set; }

        public double GasVelocity { get; set; }

        public double AverageHeatOfGas { get; set; }

        public double MaterialConsumption { get; set; }

        public double HeatOfMaterial { get; set; }

        public double HeatCoefficient { get; set; }

        public double DeviceDiameter { get; set; }


        public TeploobmenLib(TeploobmenInput input)
        {
            H = input.H;
            InitTempOfMaterial = input.InitTempOfMaterial;
            InitTempOfGas = input.InitTempOfGas;
            GasVelocity = input.GasVelocity;
            AverageHeatOfGas = input.AverageHeatOfGas;
            MaterialConsumption = input.MaterialConsumption;
            HeatOfMaterial = input.HeatOfMaterial;
            HeatCoefficient = input.HeatCoefficient;
            DeviceDiameter = input.DeviceDiameter;
        }
        //    List<double> list = new List<double>(7 * 10);
        //    int size1 = MyArr.GetLength(1);
        //    int size0 = MyArr.GetLength(0);
        //    for (i = 5; i < size0; i++)
        //    {
        //        for (j = 0; j < size1; j++)
        //            list.Add(MyArr[i, j]);
        //    }
        //    //return (list));
        //    return ;
        //}
        public TeploobmenOutput Solve()
        {
            //int i = 8, j;
            //double[,] MyArr = new double[8, H * 2 + 1];

            //double RatioHeatOfTheFlows = (HeatOfMaterial * MaterialConsumption) / (AverageHeatOfGas * GasVelocity * 3.14);

            //double TotalRelativeHeight = (HeatCoefficient * H) / (GasVelocity * AverageHeatOfGas * 1000);

            //double DopHelp = 1 - RatioHeatOfTheFlows * Math.Exp((-(1 - RatioHeatOfTheFlows) * TotalRelativeHeight) / RatioHeatOfTheFlows);

            //for (i = 0; i <= 7; i++)
            //{
            //    for (j = 0; j < H*2+1; j++)
            //    {
            //        if ((i == 0) && (j < 1))
            //        {
            //            MyArr[i, j] = (HeatCoefficient * j / 2) / (GasVelocity * AverageHeatOfGas * 1000);

            //        }
            //        else if ((i == 0))
            //        {
            //            double pol = Convert.ToDouble(j);
            //            MyArr[i, j] = (HeatCoefficient * (pol / 2)) / (GasVelocity * AverageHeatOfGas * 1000);
            //        }
            //        else if (i == 1)
            //        {
            //            MyArr[i, j] = 1 - Math.Exp(((RatioHeatOfTheFlows - 1) * MyArr[i - 1, j]) / RatioHeatOfTheFlows);

            //        }

            //        else if (i == 2)
            //        {
            //            MyArr[i, j] = 1 - RatioHeatOfTheFlows * Math.Exp(((RatioHeatOfTheFlows - 1) * MyArr[i - 2, j]) / RatioHeatOfTheFlows);
            //        }

            //        else if (i == 3)
            //        {
            //            MyArr[i, j] = MyArr[i - 2, j] / DopHelp;
            //        }

            //        else if (i == 4)
            //        {
            //            MyArr[i, j] = MyArr[i - 2, j] / DopHelp;
            //        }

            //        else if (i == 5)
            //        {
            //            MyArr[i, j] = InitTempOfMaterial + (InitTempOfGas - InitTempOfMaterial) * MyArr[i - 2, j];
            //        }

            //        else if (i == 6)
            //        {
            //            MyArr[i, j] = InitTempOfMaterial + (InitTempOfGas - InitTempOfMaterial) * MyArr[i - 2, j];
            //        }

            //        else if (i == 7)
            //        {
            //            MyArr[i, j] = MyArr[i - 1, j] - MyArr[i - 2, j];
            //        }

            //    }

            //}
            List<double> ys = new List<double>();
            for (int i =0; i< H * 2 + 1; i++)
            {
                double pol = Convert.ToDouble(i);
                ys.Add(pol / 2);
            }
            var model = new TeploobmenOutput()
            {
                Rows = new List<TeploobmenOutputRow>()
            };
            foreach (var y in ys)
            {
                var row = new TeploobmenOutputRow
                {
                    Y = y,
                    str1 = getStr1(y),
                    str2 = (getStr2(y)),
                    str3 = getStr3(y),
                    str4 = getStr4(y),
                    str5 = getStr5(y),
                    str6 = getStr6(y),
                    str7 = getStr7(y),
                    str8 = getStr8(y)
                };
                model.Rows.Add(row);
            }
            return model;
            //return TeploobmenOutput
            //{
            //    MyArray = MyArr
            //};
        }
        public double getRatioHeatOfTheFlows()
        {
            return (HeatOfMaterial* MaterialConsumption) / (AverageHeatOfGas* GasVelocity * 3.14);
        }
        private double getTotalRelativeHeight()
        {
            return (HeatCoefficient * H) / (GasVelocity * AverageHeatOfGas * 1000);
        }
        private double getDopHelp()
        {
            double TotalRelativeHeight = getTotalRelativeHeight();
            double RatioHeatOfTheFlows = getRatioHeatOfTheFlows();
            return 1 - RatioHeatOfTheFlows * Math.Exp((-(1 - RatioHeatOfTheFlows) * TotalRelativeHeight) / RatioHeatOfTheFlows);
        }

        private double getStr1(double y)
        {
            return ((HeatCoefficient * y) / (GasVelocity * AverageHeatOfGas * 1000));

        }
        private double getStr2(double y)
        {
            double RatioHeatOfTheFlows = getRatioHeatOfTheFlows();
            return 1 - Math.Exp(((RatioHeatOfTheFlows - 1) * getStr1(y)) / RatioHeatOfTheFlows);
        }
        private double getStr3(double y)
        {
            double RatioHeatOfTheFlows = getRatioHeatOfTheFlows();
            return 1 - RatioHeatOfTheFlows * Math.Exp(((RatioHeatOfTheFlows - 1) * getStr1(y)) / RatioHeatOfTheFlows);
        }
        private double getStr4(double y)
        {
            double DopHelp = getDopHelp();
            return getStr2(y) / DopHelp;
        }
        private double getStr5(double y)
        {
            double DopHelp = getDopHelp();
            return getStr3(y) / DopHelp;
        }
        private double getStr6(double y)
        {
            return InitTempOfMaterial + (InitTempOfGas - InitTempOfMaterial) * getStr4(y);
        }
        private double getStr7(double y)
        {
            return InitTempOfMaterial + (InitTempOfGas - InitTempOfMaterial) * getStr5(y);
        }
        private double getStr8(double y)
        {
            return getStr7(y) - getStr6(y);
        }
    }
}