﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teploobmen.lib
{
    public class TeploobmenInput
    {
        public int H { get; set; }

        public double InitTempOfMaterial { get; set; }

        public double InitTempOfGas { get; set; }

        public double GasVelocity { get; set; }

        public double AverageHeatOfGas { get; set; }

        public double MaterialConsumption { get; set; }

        public double HeatOfMaterial { get; set; }

        public double HeatCoefficient { get; set; }

        public double DeviceDiameter { get; set; }
        public string Name { get; set; }
    }
}
